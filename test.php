<?php

$url = $_POST['url'];

if ($url != ''){
	if (substr($url, 0, 4) != 'http'){
		$url = base64_decode($url);
	}
        if (substr($url, 0, 4) == 'http'){
                if (isset($_POST['only_header'])){
					if (isset($_POST['cookie_file'])){
						echo curl_it_header($url, '', $_POST['cookie_file']);
					}else{
						echo curl_it_header($url);
					}
				}else{
					if (isset($_POST['cookie_file'])){
						echo curl_it($url, '', $_POST['cookie_file']);
					}else{
						echo curl_it($url);
					}
				}
        }
}

function curl_it($url, $post_var='', $cookie='', $referer='', &$response_code=null)
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	curl_setopt($ch, CURLOPT_ENCODING , "gzip");

	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);

	if ($cookie != '') {
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	}
	if (isset($_POST['cookie'])){
		curl_setopt($ch, CURLOPT_COOKIE, $_POST['cookie']);
	}
	if (isset($_POST['$referer'])){
		curl_setopt($ch, CURLOPT_REFERER, $_POST['$referer']);
	}
	if (isset($_POST['proxy'])){
		curl_setopt($ch, CURLOPT_PROXY, $_POST['proxy']);
	}
	
                                if (isset($_POST['post'])){
									curl_setopt($ch, CURLOPT_POST, 1);
                                        curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST['post']);
                                }


	if ($referer != ''){
			curl_setopt($ch, CURLOPT_REFERER, $referer);
	}

	$result = curl_exec($ch);
	if ($result === false){
			$result = 'Curl error: ' . curl_error($ch);
	}
	$response_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);

	return $result;
}


function curl_it_header($url, $post_var='', $cookie='', $referer='')
{
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)");
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_HEADER, 1);
	curl_setopt($ch, CURLOPT_NOBODY, 1);

	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_setopt($ch, CURLOPT_TIMEOUT, 10);

	if ($cookie != '') {
			curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
	}
	if (isset($_POST['cookie'])){
		curl_setopt($ch, CURLOPT_COOKIE, $_POST['cookie']);
	}
	if (isset($_POST['$referer'])){
		curl_setopt($ch, CURLOPT_REFERER, $_POST['$referer']);
	}

	if ($referer != ''){
			curl_setopt($ch, CURLOPT_REFERER, $referer);
	}

	$result = curl_exec($ch);

	curl_close($ch);

	return $result;
}
